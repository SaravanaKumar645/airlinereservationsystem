package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.RouteDto;
import com.backend.airlinereservationsystem.Entity.Route;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Repository.RouteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.backend.airlinereservationsystem.Mapper.RouteMapper.routeDtoObjectMapper;
import static com.backend.airlinereservationsystem.Mapper.RouteMapper.routeObjectMapper;

@Service
@RequiredArgsConstructor
@Transactional
public class RouteService {
    private final RouteRepository routeRepository;

    public RouteDto saveRouteDetails(RouteDto routeDto) {
        Route savedRoute = routeRepository.save(routeObjectMapper(routeDto));
        return routeDtoObjectMapper(savedRoute);
    }

    public RouteDto getRouteDetail(RouteDto routeDto) {
        Optional<Route> fetchedRoute = routeRepository.findBySourceAndDestination(routeDto.getSource(), routeDto.getDestination());
        if (fetchedRoute.isPresent()) {
            return routeDtoObjectMapper(fetchedRoute.get());
        }
        throw new ResourceNotFoundException("Route with given details not exists !");
    }
}
