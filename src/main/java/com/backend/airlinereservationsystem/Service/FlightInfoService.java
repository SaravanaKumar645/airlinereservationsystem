package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.AllFlightDetailsDto;
import com.backend.airlinereservationsystem.Dto.FlightInfoDto;
import com.backend.airlinereservationsystem.Entity.AirlineInfo;
import com.backend.airlinereservationsystem.Entity.FlightInfo;
import com.backend.airlinereservationsystem.Entity.Route;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Mapper.FlightInfoMapper;
import com.backend.airlinereservationsystem.Repository.AirlineInfoRepository;
import com.backend.airlinereservationsystem.Repository.FlightInfoRepository;
import com.backend.airlinereservationsystem.Repository.RouteRepository;
import com.backend.airlinereservationsystem.Repository.SeatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class FlightInfoService {
    private final FlightInfoRepository flightInfoRepository;
    private final AirlineInfoRepository airlineInfoRepository;
    private final RouteRepository routeRepository;
    private final SeatRepository seatRepository;
    private final SeatService seatService;

    public FlightInfoDto addFlightDetails(FlightInfoDto flightInfoDto){
        Optional<AirlineInfo> airlineInfo=airlineInfoRepository.findById(flightInfoDto.getAirlineId());
        Optional<Route> routeInfo=routeRepository.findById(flightInfoDto.getRouteId());
        if(airlineInfo.isPresent() && routeInfo.isPresent()){
            FlightInfo savedFlight=flightInfoRepository.save(FlightInfoMapper
                    .flightInfoObjectMapper(
                            flightInfoDto,
                            airlineInfo.get(),
                            routeInfo.get()
                    )
            );
            seatService.addSeatsUnderFlightId(flightInfoDto.getSeatDtoList(),savedFlight.getId());
            return FlightInfoMapper.flightInfoDtoObjectMapper(savedFlight);
        }
        throw new ResourceNotFoundException("Try again!. Some info not found.");
    }

    public List<AllFlightDetailsDto> fetchAllFlightDetails() {
       return flightInfoRepository.findAllFlightDetails();
    }
}
