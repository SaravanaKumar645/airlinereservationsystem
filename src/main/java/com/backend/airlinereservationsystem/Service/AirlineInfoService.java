package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.AirlineInfoDto;
import com.backend.airlinereservationsystem.Entity.AirlineInfo;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Repository.AirlineInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.backend.airlinereservationsystem.Mapper.AirlineInfoMapper.airlineInfoDtoObjectMapper;
import static com.backend.airlinereservationsystem.Mapper.AirlineInfoMapper.airlineObjectMapper;

@Service
@RequiredArgsConstructor
@Transactional
public class AirlineInfoService {
    private final AirlineInfoRepository airlineInfoRepository;

    public AirlineInfoDto saveAirlineDetail(AirlineInfoDto airlineInfoDto) {
        AirlineInfo airlineInfo = airlineInfoRepository.save(airlineObjectMapper(airlineInfoDto));
        return airlineInfoDtoObjectMapper(airlineInfo);
    }

    public AirlineInfoDto getAirlineInfoByName(AirlineInfoDto airlineInfoDto) {
        Optional<AirlineInfo> airlineInfo = airlineInfoRepository.findByAirlineName(airlineInfoDto.getAirlineName());
        if (airlineInfo.isPresent()) {
            return airlineInfoDtoObjectMapper(airlineInfo.get());
        }
        throw new ResourceNotFoundException("Airline with name: " + airlineInfoDto.getAirlineName() + " not found !");
    }
}
