package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.SeatDto;
import com.backend.airlinereservationsystem.Entity.FlightInfo;
import com.backend.airlinereservationsystem.Entity.Seat;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Mapper.SeatMapper;
import com.backend.airlinereservationsystem.Repository.FlightInfoRepository;
import com.backend.airlinereservationsystem.Repository.SeatRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.backend.airlinereservationsystem.Mapper.SeatMapper.seatObjectMapperWithFlightId;

@Service
@Transactional
@RequiredArgsConstructor
public class SeatService {
    private final SeatRepository seatRepository;
    private final FlightInfoRepository flightInfoRepository;

    public Boolean addSeatsUnderFlightId(List<SeatDto> seatDtoList,Long flightId){
        Optional<FlightInfo> fetchedFlightInfo=flightInfoRepository.findById(flightId);
        if (fetchedFlightInfo.isPresent()) {
            List<Seat> seatList = seatDtoList
                    .stream()
                    .map(seatDto -> seatObjectMapperWithFlightId(seatDto,fetchedFlightInfo.get()))
                    .collect(Collectors.toList());
            List<Seat> savedSeats = seatRepository.saveAll(seatList);
            return savedSeats.size() == seatList.size();
        }
        throw new ResourceNotFoundException("No flight with id :\""+flightId+"\" found !");
    }

    public List<SeatDto> getAllSeatUnderFlightId(Long flightId){
        System.out.println(flightId.getClass().getTypeName());
        Optional<FlightInfo> fetchedFlightInfo=flightInfoRepository.findByFlightId(Long.parseLong("10002"));
        if (fetchedFlightInfo.isPresent()){
            List<Seat> fetchedSeatList=seatRepository.findByFlightId(flightId);
            return fetchedSeatList
                    .stream()
                    .map(SeatMapper::seatDtoObjectMapper)
                    .collect(Collectors.toList());
        }
        throw new ResourceNotFoundException("No flight with id :\""+flightId+"\" found !");
    }
}
