package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.RoleDto;
import com.backend.airlinereservationsystem.Entity.Role;
import com.backend.airlinereservationsystem.Entity.User;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Repository.RoleRepository;
import com.backend.airlinereservationsystem.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static com.backend.airlinereservationsystem.Mapper.RoleMapper.roleDtoObjectMapper;
import static com.backend.airlinereservationsystem.Mapper.RoleMapper.roleObjectMapper;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class RoleService {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    public RoleDto saveRole(RoleDto roleDto){
        Role savedRole=roleRepository.save(roleObjectMapper(roleDto));
        return roleDtoObjectMapper(savedRole);
    }

    public void saveRoleToUser(String emailId, String roleName){
        Optional<User> user=userRepository.findByEmailId(emailId);
        Optional<Role> role=roleRepository.findByName(roleName);
        if (user.isPresent() && role.isPresent()){
            user.get().getUserRoles().add(role.get());
        }
        if(user.isEmpty() || role.isEmpty()){
            throw new ResourceNotFoundException("Check your Email or Role and try again !");
        }
    }
}
