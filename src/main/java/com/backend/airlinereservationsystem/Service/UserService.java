package com.backend.airlinereservationsystem.Service;

import com.backend.airlinereservationsystem.Dto.UserDto;
import com.backend.airlinereservationsystem.Entity.Role;
import com.backend.airlinereservationsystem.Entity.User;
import com.backend.airlinereservationsystem.Exceptions.DuplicateEntryException;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotFoundException;
import com.backend.airlinereservationsystem.Mapper.UserMapper;
import com.backend.airlinereservationsystem.Repository.RoleRepository;
import com.backend.airlinereservationsystem.Repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static com.backend.airlinereservationsystem.Mapper.UserMapper.userDtoObjectMapperWithRole;
import static com.backend.airlinereservationsystem.Mapper.UserMapper.userObjectMapper;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByEmailId(email);
        if (user.isPresent()){
            Collection<SimpleGrantedAuthority> authorities=new HashSet<>();
            user.get().getUserRoles()
                    .forEach(role -> authorities.add(new SimpleGrantedAuthority(role.getName())));

            return new org.springframework.security.core.userdetails.User(
                    user.get().getEmailId(),
                    String.valueOf(user.get().getPassword()),
                    authorities
                    );
        }
        throw new UsernameNotFoundException("User with email :"+email+" not found");
    }


    public UserDto saveUser(UserDto userDto) {
        Optional<User> user=userRepository.findByEmailId(userDto.getEmailId());
        Optional<Role> role=roleRepository.findByName("ROLE_USER");
        if(user.isEmpty() && role.isPresent()){
            userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
            User savedUser=userRepository.save(userObjectMapper(userDto));
            savedUser.getUserRoles().add(role.get());
            return userDtoObjectMapperWithRole(savedUser);
        }
        throw new DuplicateEntryException("User with email: "+userDto.getEmailId()+" already exists !");
    }

    public UserDto getUser(String email) {
        Optional<User> user =userRepository.findByEmailId(email);
        log.info(user.toString());
        if(user.isPresent()){
            return userDtoObjectMapperWithRole(user.get());
        }
        throw new ResourceNotFoundException("User with email :"+email+" not found");
    }

    public List<UserDto> getAllUsers(){
        List<User> userList=userRepository.findAll();
        return userList
                .stream()
                .map(UserMapper::userDtoObjectMapperWithRole)
                .collect(Collectors.toList());
    }


}
