package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.AirlineInfoDto;
import com.backend.airlinereservationsystem.Service.AirlineInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/airline")
public class AirlineController {
    private final AirlineInfoService airlineService;

    @PostMapping(path = "/save")
    public ResponseEntity<AirlineInfoDto> saveAirlineInfo(@Valid @RequestBody AirlineInfoDto airlineInfoDto){
       AirlineInfoDto savedAirlineInfo= airlineService.saveAirlineDetail(airlineInfoDto);
       return ResponseEntity.status(HttpStatus.CREATED).body(savedAirlineInfo);
    }

    @GetMapping(path = "/get")
    public ResponseEntity<AirlineInfoDto> getAirlineInfo(@Valid @RequestBody AirlineInfoDto airlineInfoDto){
        AirlineInfoDto fetchedAirlineInfo=airlineService.getAirlineInfoByName(airlineInfoDto);
        return ResponseEntity.status(HttpStatus.FOUND).body(fetchedAirlineInfo);
    }
}
