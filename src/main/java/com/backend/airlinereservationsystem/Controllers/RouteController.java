package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.RouteDto;
import com.backend.airlinereservationsystem.Service.RouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/flightRoute")
@RequiredArgsConstructor
public class RouteController {

    private final RouteService routeService;

    @PostMapping(path = "/save")
    public ResponseEntity<RouteDto> saveAirlineInfo(@Valid @RequestBody RouteDto routeDto) {
        RouteDto savedRouteDetails = routeService.saveRouteDetails(routeDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedRouteDetails);
    }

    @GetMapping(path = "/get")
    public ResponseEntity<RouteDto> getRouteBySourceAndDestination(@Valid @RequestBody RouteDto routeDto) {
        RouteDto fetchedRouteDetail = routeService.getRouteDetail(routeDto);
        return ResponseEntity.status(HttpStatus.FOUND).body(fetchedRouteDetail);
    }
}
