package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.AllFlightDetailsDto;
import com.backend.airlinereservationsystem.Dto.FlightInfoDto;
import com.backend.airlinereservationsystem.Service.FlightInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/flights")
public class FlightInfoController {

    private final FlightInfoService flightInfoService;

    @PostMapping(path = "/add")
    public ResponseEntity<FlightInfoDto> addFlightDetails(@Valid @RequestBody FlightInfoDto flightInfoDto){
        FlightInfoDto flightDetailsDto=flightInfoService.addFlightDetails(flightInfoDto);
       return ResponseEntity.status(HttpStatus.CREATED).body(flightDetailsDto);
    }

    @GetMapping(path = "/getAll")
    public ResponseEntity<List<AllFlightDetailsDto>> getAllFlightDetails(){
        List<AllFlightDetailsDto> flightDetailsList=flightInfoService.fetchAllFlightDetails();
        return ResponseEntity.status(HttpStatus.FOUND).body(flightDetailsList);
    }
}
