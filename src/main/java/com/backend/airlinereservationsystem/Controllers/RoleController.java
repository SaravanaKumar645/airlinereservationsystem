package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.RoleDto;
import com.backend.airlinereservationsystem.Exceptions.EmailNotValidException;
import com.backend.airlinereservationsystem.Exceptions.ResourceNotValidException;
import com.backend.airlinereservationsystem.Service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(path = "/api/role")
@RequiredArgsConstructor
public class RoleController {
    private final RoleService roleService;

    @PostMapping(path = "/save")
    public ResponseEntity<RoleDto> saveRole(@Valid @RequestBody RoleDto roleDto){
        return ResponseEntity.status(HttpStatus.CREATED).body(roleService.saveRole(roleDto));
    }

    @PostMapping(path = "/add-to-user")
    public ResponseEntity<?> addRoleToUser(@RequestBody Map<String,String> roleDetails) {
        String emailId=roleDetails.get("emailId");
        String roleName=roleDetails.get("roleName");
        if(!emailId.matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")){
            throw new EmailNotValidException("Must be a valid email !");
        }else if( roleName.isBlank()){
            throw new ResourceNotValidException("Role name cannot be empty");
        }
        roleService.saveRoleToUser(emailId, roleName);
        return ResponseEntity.ok().body("Role added");
    }
}
