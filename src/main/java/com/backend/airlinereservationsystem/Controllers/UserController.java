package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.UserDto;
import com.backend.airlinereservationsystem.Exceptions.EmailNotValidException;
import com.backend.airlinereservationsystem.Service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(path = "/welcome")
    public String helloMessage(){
        return "Welcome User !";
    }

    @PostMapping(path = "/register-user")
    public ResponseEntity<UserDto> registerUser(@Valid @RequestBody UserDto userDto) {
        UserDto savedUser = userService.saveUser(userDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @PostMapping(path = "/get-user")
    public ResponseEntity<UserDto> loginUser(@RequestBody Map<String,String> requestBody) {
        if(!requestBody.get("emailId").matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")){
            throw new EmailNotValidException("Must be a valid email !");
        }
        UserDto fetchedUser = userService.getUser(requestBody.get("emailId"));
        return ResponseEntity.status(HttpStatus.FOUND).body(fetchedUser);
    }

    @GetMapping(path = "/all-users")
    public ResponseEntity<List<UserDto>> getAllUsers(){
        return ResponseEntity.ok().body(userService.getAllUsers());
    }


}
