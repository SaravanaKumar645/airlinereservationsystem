package com.backend.airlinereservationsystem.Controllers;

import com.backend.airlinereservationsystem.Dto.CustomResponseDto;
import com.backend.airlinereservationsystem.Dto.SeatDto;
import com.backend.airlinereservationsystem.Service.SeatService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/api/seats")
@RequiredArgsConstructor
public class SeatController {
    private final SeatService seatService;

    @PostMapping(path ="/add")
    public ResponseEntity<CustomResponseDto> addSeatsToFlight(@Valid @RequestBody List<SeatDto> seatDto, @RequestParam Long flightId){
        Boolean savedStatus=seatService.addSeatsUnderFlightId(seatDto,flightId);
        String message=savedStatus?"Added seat details":"Try again. Details not added !";
        return ResponseEntity.status(HttpStatus.CREATED).body(CustomResponseDto.createCustomResponse(
                HttpStatus.CREATED.toString(),
                !savedStatus,
                message,
                null
        ));
    }

    @GetMapping(path = "/getAll")
    public ResponseEntity<List<SeatDto>> getAllSeatsByFlightId(@RequestParam("flightId") Long flightId){
        List<SeatDto> seatDtoList=seatService.getAllSeatUnderFlightId(flightId);
        return ResponseEntity.status(HttpStatus.FOUND).body(seatDtoList);
    }
}
