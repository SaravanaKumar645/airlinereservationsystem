package com.backend.airlinereservationsystem.Enum;

public enum Gender {
    MALE,
    FEMALE,
    OTHERS
}
