package com.backend.airlinereservationsystem.Enum;

public enum SeatType {
    PREMIUM,
    BUSINESS,
    ECONOMIC
}
