package com.backend.airlinereservationsystem.Enum;

public enum BookingStatus {
    BOOKED,
    AVAILABLE,
}
