package com.backend.airlinereservationsystem.Config;

import com.backend.airlinereservationsystem.Filter.CustomAuthenticationFilter;
import com.backend.airlinereservationsystem.Filter.CustomAuthorizationFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurityConfig(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter=new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/user/login");
        //enable cors and disable csrf
        http
                .cors().configurationSource(corsConfigurationSource())
                .and()
                .csrf()
                .disable();

        //set session management as stateless to not track users by cookie
        http
                .sessionManagement()
                .sessionCreationPolicy(STATELESS).and();

        //adding authentication filter
        http
                .addFilter(customAuthenticationFilter)
                //.addFilter(corsFilter())
                .addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);


        //set unauthorized requests exception handler
//        http
//                .exceptionHandling()
//                .accessDeniedHandler((request, response, accessDeniedException) -> response.sendError(
//                        HttpServletResponse.SC_FORBIDDEN,
//                        accessDeniedException.getMessage())
//                )
//                .authenticationEntryPoint((request, response, authException) -> response.sendError(
//                        HttpServletResponse.SC_UNAUTHORIZED,
//                        authException.getMessage())
//                )
//                .and();
        //set permission on endpoints
        http
                .authorizeRequests()
                .antMatchers("/api/user/welcome").hasAnyAuthority("ROLE_USER","ROLE_ADMIN")
                .antMatchers("/api/user/login").permitAll()
                .antMatchers("/api/user/register-user").permitAll()
                .antMatchers("/api/role/**").hasAnyAuthority("ROLE_ADMIN")
                .antMatchers("/api/airline/**").hasAnyAuthority("ROLE_ADMIN")
                .anyRequest().authenticated();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    //
    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source =
                new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();

        config.setAllowedOriginPatterns(List.of("*"));
        config.setAllowedMethods(Collections.singletonList("*"));
        config.setAllowedHeaders(Collections.singletonList("*"));
        config.setAllowCredentials(true);

        source.registerCorsConfiguration("/**", config);
        return source;
        //return new CorsFilter(source);
    }
}
