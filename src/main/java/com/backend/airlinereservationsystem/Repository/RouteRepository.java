package com.backend.airlinereservationsystem.Repository;

import com.backend.airlinereservationsystem.Entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {

    @Query("select fr from Route fr where fr.source=:source and fr.destination=:destination")
    Optional<Route> findBySourceAndDestination(String source, String destination);
}
