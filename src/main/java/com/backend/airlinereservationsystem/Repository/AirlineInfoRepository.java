package com.backend.airlinereservationsystem.Repository;

import com.backend.airlinereservationsystem.Entity.AirlineInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AirlineInfoRepository extends JpaRepository<AirlineInfo,Long> {
    @Query("select ai from AirlineInfo ai where upper(?1)=upper(ai.airlineName)")
    Optional<AirlineInfo> findByAirlineName(String airlineName);
}
