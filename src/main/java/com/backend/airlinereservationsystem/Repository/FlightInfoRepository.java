package com.backend.airlinereservationsystem.Repository;

import com.backend.airlinereservationsystem.Dto.AllFlightDetailsDto;
import com.backend.airlinereservationsystem.Entity.FlightInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FlightInfoRepository extends JpaRepository<FlightInfo,Long> {
    @Query(value = "select fi.id flightId,fi.flight_name flightName,fi.departure_time departureTime,fi.arrival_time arrivalTime,\n" +
            "    fi.start_date startDate,fi.expiry_date expiryDate,fi.total_seat_count seatCount,\n" +
            "    ai.airline_id airlineId,ai.airline_name airlineName,fr.id routeId,fr.source,fr.destination\n" +
            "    from flight_info fi\n" +
            "    inner join airline_info ai on fi.airline_id =ai.airline_id\n" +
            "    inner join flight_routes fr on fi.route_id = fr.id;",
            nativeQuery = true)
    List<AllFlightDetailsDto> findAllFlightDetails();
    @Query(value = "select * from flight_info fi where fi.id=?1",nativeQuery = true)
    Optional<FlightInfo> findByFlightId(Long id);

}
