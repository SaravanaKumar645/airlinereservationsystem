package com.backend.airlinereservationsystem.Repository;

import com.backend.airlinereservationsystem.Entity.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface SeatRepository extends JpaRepository<Seat,Long> {

    @Query(value = "select * from seat_details sd where sd.flight_id=?1",nativeQuery = true)
    List<Seat> findByFlightId(Long flightId);
}