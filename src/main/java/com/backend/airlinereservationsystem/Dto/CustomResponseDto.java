package com.backend.airlinereservationsystem.Dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@NoArgsConstructor
public class CustomResponseDto {
    private String status;
    private Boolean error;
    private String message;
    private Object data;

    public static CustomResponseDto createCustomResponse(String status ,Boolean error,String message,Object data){
        CustomResponseDto customResponseDto=new CustomResponseDto();
        customResponseDto.setStatus(status);
        customResponseDto.setError(error);
        customResponseDto.setMessage(message);
        customResponseDto.setData(data);
        return customResponseDto;
    }
}

