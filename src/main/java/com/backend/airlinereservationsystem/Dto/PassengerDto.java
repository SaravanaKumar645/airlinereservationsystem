package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PassengerDto {
    private Long passengerId;

    @NotBlank(message = "\"First Name\" cannot be empty !")
    private String firstName;

    @NotBlank(message = "\"Last Name\" cannot be empty !")
    private String lastName;

    @NotBlank(message = "\"Gender\" cannot be empty !")
    private String gender;

    @NotBlank(message = "\"Date of Birth\" cannot be empty !")
    private String dateOfBirth;


}
