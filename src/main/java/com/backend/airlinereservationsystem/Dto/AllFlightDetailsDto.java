package com.backend.airlinereservationsystem.Dto;

import org.springframework.beans.factory.annotation.Value;

public interface AllFlightDetailsDto {
    @Value("#{@flightInfoDto.buildFlightDto(target.flightId,target.flightName,target.departureTime,target.arrivalTime,target.seatCount,target.startDate,target.expiryDate)}")
    FlightInfoDto getFlightInfo();
    @Value("#{@airlineInfoDto.buildAirlineInfoDto(target.airlineId,target.airlineName)}")
    AirlineInfoDto getAirlineInfo();
    @Value("#{@routeDto.buildRouteDto(target.routeId,target.source,target.destination)}")
    RouteDto getRouteInfo();
}

