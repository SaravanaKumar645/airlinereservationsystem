package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Component
public class FlightInfoDto {
    private Long id;

    @NotBlank(message = "Flight name cannot be empty !")
    @Size(max = 30,message = "Flight should not exceed 30 characters !")
    private String flightName;

    @NotBlank(message = "Flight departure time cannot be empty !")
    private String departureTime;

    @NotBlank(message = "Flight arrival time cannot be empty !")
    private String arrivalTime;

    @NotNull(message = "Field cannot be empty !")
    private Integer totalSeatCount;


    @NotBlank(message = "Start date cannot be empty")
    private String startDate;

    @NotBlank(message = "Final boarding date cannot be empty")
    private String expiryDate;

    @NotNull(message = "Route id must be provided !")
    private Long routeId;

    @NotNull(message = "Route id must be provided !")
    private Long airlineId;

    private List<SeatDto> seatDtoList;


    public FlightInfoDto buildFlightDto(Long id,String flightName,String departureTime,
                                        String arrivalTime,Integer totalSeatCount,
                                        Date startDate,Date expiryDate){

        return FlightInfoDto
                .builder()
                .id(id)
                .flightName(flightName)
                .departureTime(departureTime)
                .arrivalTime(arrivalTime)
                .totalSeatCount(totalSeatCount)
                .startDate(startDate.toString())
                .expiryDate(expiryDate.toString())
                .build();

    }
}
