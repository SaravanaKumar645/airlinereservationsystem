package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;

    @NotBlank(message = "Name cannot be empty !")
    @Size(max = 30)
    private String fullName;

    @Email(message = "Please enter a valid email !")
    private String emailId;

    @NotBlank(message = "Password cannot be empty !")
    @Size(min = 4,max = 45,message = "Password should meet the requirements( min 4 characters - max 45 characters )")
    private String password;

    @NotBlank(message = "Mobile number cannot be empty !")
    @Size(max = 10,message = "Mobile number must be equal to length of 10.")
    private String mobileNumber;

    private Set<RoleDto> userRoleList;
}
