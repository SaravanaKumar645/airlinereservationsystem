package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class RouteDto {
    private Long id;

    @NotBlank(message = "Source cannot be empty !")
    @Size(max = 50,message = "Source name cannot exceed 50 characters !")
    private String source;

    @NotBlank(message = "Destination cannot be empty !")
    @Size(max = 50,message = "Destination name cannot exceed 50 characters !")
    private String destination;

    public RouteDto buildRouteDto(Long id,String source, String destination){
        return RouteDto
                .builder()
                .id(id)
                .source(source)
                .destination(destination)
                .build();
    }
}
