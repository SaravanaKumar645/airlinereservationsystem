package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class SeatDto {
    private Long id;

    @NotBlank(message = "Seat number cannot be null !")
    private String seatNo;

    @NotBlank(message = "Type must be one of these : \"PREMIUM\",\"BUSINESS\",\"NORMAL\",")
    private String seatType;

    @NotNull(message = "Seat fare needs to be included !")
    private Long seatFare;

    private String seatStatus;

    public SeatDto buildSeatDto(Long id, String seatNo,
                                Long seatFare,String seatType,
                                String seatStatus){
        return SeatDto
                .builder()
                .id(id)
                .seatNo(seatNo)
                .seatFare(seatFare)
                .seatType(seatType)
                .seatStatus(seatStatus)
                .build();
    }

}
