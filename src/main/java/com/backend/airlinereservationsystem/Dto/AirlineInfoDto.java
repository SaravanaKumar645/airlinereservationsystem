package com.backend.airlinereservationsystem.Dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Component
public class AirlineInfoDto {
    private Long airlineId;

    @NotBlank(message = "Airline name cannot be empty !")
    @Size(max = 50,message = "Airline name cannot exceed 50 characters !")
    private String airlineName;

    public AirlineInfoDto buildAirlineInfoDto(Long id, String name){
        return AirlineInfoDto
                .builder()
                .airlineId(id)
                .airlineName(name)
                .build();
    }
}
