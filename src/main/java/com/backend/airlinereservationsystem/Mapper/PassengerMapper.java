package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.PassengerDto;
import com.backend.airlinereservationsystem.Entity.Passenger;
import com.backend.airlinereservationsystem.Enum.Gender;

import java.time.LocalDate;

public class PassengerMapper {
    public static PassengerDto passengerDtoMapper(Passenger passenger){
        return PassengerDto
                .builder()
                .passengerId(passenger.getPassengerId())
                .firstName(passenger.getFirstName())
                .lastName(passenger.getLastName())
                .gender(passenger.getGender().toString())
                .dateOfBirth(passenger.getDateOfBirth().toString())
                .build();
    }
    public static Passenger passengerMapper(PassengerDto passengerDto){
        return Passenger
                .builder()
                .firstName(passengerDto.getFirstName())
                .lastName(passengerDto.getLastName())
                .gender(Gender.valueOf(passengerDto.getGender()))
                .dateOfBirth(LocalDate.parse(passengerDto.getDateOfBirth()))
                .build();
    }
}
