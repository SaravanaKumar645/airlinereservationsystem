package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.AirlineInfoDto;
import com.backend.airlinereservationsystem.Entity.AirlineInfo;

public class AirlineInfoMapper {
    public static AirlineInfo airlineObjectMapper(AirlineInfoDto airlineInfoDto){
        return AirlineInfo
                .builder()
                .airlineName(airlineInfoDto.getAirlineName())
                .build();
    }
    public static AirlineInfoDto airlineInfoDtoObjectMapper(AirlineInfo airlineInfo){
        return AirlineInfoDto
                .builder()
                .airlineId(airlineInfo.getAirlineId())
                .airlineName(airlineInfo.getAirlineName())
                .build();
    }
}
