package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.SeatDto;
import com.backend.airlinereservationsystem.Entity.FlightInfo;
import com.backend.airlinereservationsystem.Entity.Seat;
import com.backend.airlinereservationsystem.Enum.BookingStatus;
import com.backend.airlinereservationsystem.Enum.SeatType;

public class SeatMapper {
    public static Seat seatObjectMapper(SeatDto seatDto){
        return Seat
                .builder()
                .seatNo(seatDto.getSeatNo())
                .seatType(SeatType.valueOf(seatDto.getSeatType()))
                .seatFare(seatDto.getSeatFare())
                .build();
    }
    public static SeatDto seatDtoObjectMapper(Seat seat){
        return SeatDto
                .builder()
                .id(seat.getId())
                .seatNo(seat.getSeatNo())
                .seatType(seat.getSeatType().toString())
                .seatFare(seat.getSeatFare())
                .seatStatus(seat.getSeatStatus().toString())
                .build();
    }
    public static Seat seatObjectMapperWithFlightId(SeatDto seatDto, FlightInfo flightInfo){
        return Seat
                .builder()
                .seatNo(seatDto.getSeatNo())
                .seatType(SeatType.valueOf(seatDto.getSeatType()))
                .seatFare(seatDto.getSeatFare())
                .seatStatus(BookingStatus.AVAILABLE)
                .flightInfo(flightInfo)
                .build();
    }
}
