package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.RoleDto;
import com.backend.airlinereservationsystem.Entity.Role;

public class RoleMapper {
    public static Role roleObjectMapper(RoleDto roleDto){
        return Role.builder()
                .name(roleDto.getName())
                .build();
    }
    public static RoleDto roleDtoObjectMapper(Role role){
        return RoleDto.builder()
                .id(role.getId())
                .name(role.getName())
                .build();
    }
}
