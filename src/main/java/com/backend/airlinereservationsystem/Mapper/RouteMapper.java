package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.RouteDto;
import com.backend.airlinereservationsystem.Entity.Route;

public class RouteMapper {
    public static Route routeObjectMapper(RouteDto routeDto) {
        return Route
                .builder()
                .source(routeDto.getSource())
                .destination(routeDto.getDestination())
                .build();
    }

    public static RouteDto routeDtoObjectMapper(Route route) {
        return RouteDto
                .builder()
                .id(route.getId())
                .source(route.getSource())
                .destination(route.getDestination())
                .build();
    }
}
