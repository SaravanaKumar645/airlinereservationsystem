package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.FlightInfoDto;
import com.backend.airlinereservationsystem.Entity.AirlineInfo;
import com.backend.airlinereservationsystem.Entity.FlightInfo;
import com.backend.airlinereservationsystem.Entity.Route;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class FlightInfoMapper {
    private final static String dateFormat="dd/MM/yyyy";
    public static FlightInfo flightInfoObjectMapper(FlightInfoDto flightInfoDto, AirlineInfo airlineInfo, Route route){
        return FlightInfo
                .builder()
                .flightName(flightInfoDto.getFlightName())
                .arrivalTime(flightInfoDto.getArrivalTime())
                .departureTime(flightInfoDto.getDepartureTime())
                .startDate(LocalDate.parse(flightInfoDto.getStartDate()))
                .expiryDate(LocalDate.parse(flightInfoDto.getExpiryDate()))
                .totalSeatCount(flightInfoDto.getTotalSeatCount())
                .airlineInfo(airlineInfo)
                .route(route)
                .build();
    }
    public static FlightInfoDto flightInfoDtoObjectMapper(FlightInfo flightInfo){
        return FlightInfoDto
                .builder()
                .id(flightInfo.getId())
                .flightName(flightInfo.getFlightName())
                .startDate(flightInfo.getStartDate().toString())
                .expiryDate(flightInfo.getExpiryDate().toString())
                .arrivalTime(flightInfo.getArrivalTime())
                .departureTime(flightInfo.getDepartureTime())
                .totalSeatCount(flightInfo.getTotalSeatCount())
                .build();
    }
}
