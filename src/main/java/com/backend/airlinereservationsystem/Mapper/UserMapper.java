package com.backend.airlinereservationsystem.Mapper;

import com.backend.airlinereservationsystem.Dto.UserDto;
import com.backend.airlinereservationsystem.Entity.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

public class UserMapper {

    public static User userObjectMapper(UserDto userDTO) {
        return User.builder()
                .emailId(userDTO.getEmailId())
                .fullName(userDTO.getFullName())
                .password(userDTO.getPassword().toCharArray())
                .mobileNumber(userDTO.getMobileNumber())
                .userRoles(new HashSet<>())
                .build();
    }

    public static UserDto userDtoObjectMapperWithRole(User userEntity) {
        return UserDto.builder()
                .id(userEntity.getId())
                .emailId(userEntity.getEmailId())
                .fullName(userEntity.getFullName())
                .password(null)
                .mobileNumber(userEntity.getMobileNumber())
                .userRoleList(userEntity
                        .getUserRoles()
                        .stream().map(RoleMapper::roleDtoObjectMapper)
                        .collect(Collectors.toSet()))
                .build();
    }
    public static UserDto userDtoObjectMapper(User userEntity) {
        return UserDto.builder()
                .id(userEntity.getId())
                .emailId(userEntity.getEmailId())
                .fullName(userEntity.getFullName())
                .password(null)
                .mobileNumber(userEntity.getMobileNumber())
                .userRoleList(new HashSet<>())
                .build();
    }
}
