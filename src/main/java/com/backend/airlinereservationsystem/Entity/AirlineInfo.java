package com.backend.airlinereservationsystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(
        name = "airline_info",
        uniqueConstraints = @UniqueConstraint(
                name = "airline_name_unique",
                columnNames = "airline_name"
        )
)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirlineInfo {
    @Id
    @SequenceGenerator(
            name = "airline_info_sequence",
            sequenceName = "airline_info_sequence",
            allocationSize = 1,
            initialValue = 1000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "airline_info_sequence"
    )
    private Long airlineId;

    @Column(name = "airline_name",nullable = false,length = 50)
    private String airlineName;

    @OneToMany(
            mappedBy = "airlineInfo",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<FlightInfo> flightList=new ArrayList<>();

    //helper methods
    public void addFlight(FlightInfo flightInfo){
        flightList.add(flightInfo);
        flightInfo.setAirlineInfo(this);
    }
    public void removeFlight(FlightInfo flightInfo){
        flightList.remove(flightInfo);
        flightInfo.setAirlineInfo(null);
    }
}
