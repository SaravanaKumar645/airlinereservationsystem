package com.backend.airlinereservationsystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(
        name = "flight_info",
        uniqueConstraints = @UniqueConstraint(
                name = "flight_name_unique",
                columnNames = "flight_name"
        )
)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FlightInfo {
    @Id
    @SequenceGenerator(
            name = "flight_sequence",
            sequenceName = "flight_sequence",
            allocationSize = 1,
            initialValue = 20000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "flight_sequence"
    )
    private Long id;

    @Column(name = "flight_name" ,nullable = false,length = 30)
    private String  flightName;

    @Column
    private String departureTime;

    @Column
    private String arrivalTime;

    @Column
    private Integer totalSeatCount;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate expiryDate;

    @OneToMany(
            mappedBy = "flightInfo",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Seat> seatDetails=new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "airline_id")
    @JsonIgnore
    private AirlineInfo airlineInfo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id")
    @JsonIgnore
    private Route route;
}
