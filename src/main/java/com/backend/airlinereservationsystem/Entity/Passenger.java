package com.backend.airlinereservationsystem.Entity;

import com.backend.airlinereservationsystem.Enum.Gender;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "passenger_details")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Passenger {
    @Id
    @SequenceGenerator(
            name = "passenger_sequence",
            sequenceName = "passenger_sequence",
            allocationSize = 1,
            initialValue = 70000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "passenger_sequence"
    )
    private Long passengerId;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false)
    private LocalDate dateOfBirth;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "booking_id",
            referencedColumnName = "booking_id",
            foreignKey = @ForeignKey(name = "Fk_booking_id")
    )
    @JsonIgnore
    private Booking booking;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "seat_id",
            referencedColumnName = "id",
            nullable = false,
            foreignKey = @ForeignKey(name = "Fk_seat_id")
    )
    private Seat seatInfo;
}

