package com.backend.airlinereservationsystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(name = "flight_routes")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Route {
    @Id
    @SequenceGenerator(
            name = "route_sequence",
            sequenceName = "route_sequence",
            allocationSize = 1,
            initialValue = 10000
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "route_sequence"
    )
    private Long id;

    @Column(nullable = false,length = 50)
    private String source;

    @Column(nullable = false,length = 50)
    private String destination;

    @OneToMany(mappedBy = "route")
    @JsonIgnore
    private List<FlightInfo> flightInfoList;

    public void addFlightInfo(FlightInfo flightInfo){
        flightInfoList.add(flightInfo);
        flightInfo.setRoute(this);
    }
    public void removeFlightInfo(FlightInfo flightInfo){
        flightInfoList.remove(flightInfo);
        flightInfo.setRoute(null);
    }

}
