package com.backend.airlinereservationsystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "booking_details")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Booking {
    @Id
    @SequenceGenerator(
            name = "booking_sequence",
            sequenceName = "booking_sequence",
            allocationSize = 1,
            initialValue = 80000
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "booking_sequence"
    )
    private Long bookingId;

    @Column
    private UUID ticketNo;

    @Column
    private LocalDateTime createdAt;
    @Column
    private String flightName;
    @Column
    private String departureTime;
    @Column
    private String arrivalTime;
    @Column
    private String origin;
    @Column
    private String destination;
    @Column
    private Long totalPrice;

    @OneToMany(
            mappedBy = "booking",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonIgnore
    private List<Passenger> passengerList =new ArrayList<>();
}
