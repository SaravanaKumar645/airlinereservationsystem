package com.backend.airlinereservationsystem.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.FetchType.LAZY;
import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(
        name = "user_details",
        uniqueConstraints = @UniqueConstraint(
                name = "user_email_unique",
                columnNames = "email_id"))
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @SequenceGenerator(
            name = "user_sequence",
            sequenceName = "user_sequence",
            allocationSize = 1,
            initialValue = 100
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "user_sequence"
    )
    @JsonIgnore
    private Long id;

    @Column(length = 30, nullable = false)
    private String fullName;

    @Column(name = "email_id", nullable = false, unique = true)
    private String emailId;

    @Column(nullable = false)
    private char[] password;

    @Column(nullable = false, length = 10)
    private String mobileNumber;


    @ManyToMany(fetch = LAZY,
            cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH}
    )
    @JoinTable(
            name = "user_roles",
            joinColumns = @JoinColumn(
                    name = "user_id",
                    referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "Fk_user_id")
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id",
                    referencedColumnName = "id",
                    foreignKey = @ForeignKey(name = "Fk_role_id")
            )
    )
    private Set<Role> userRoles=new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return getId().equals(user.getId()) && getFullName().equals(user.getFullName()) && getEmailId().equals(user.getEmailId()) && Arrays.equals(getPassword(), user.getPassword()) && getMobileNumber().equals(user.getMobileNumber());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getId(), getFullName(), getEmailId(), getMobileNumber());
        result = 31 * result + Arrays.hashCode(getPassword());
        return result;
    }
}
