package com.backend.airlinereservationsystem.Entity;

import com.backend.airlinereservationsystem.Enum.BookingStatus;
import com.backend.airlinereservationsystem.Enum.SeatType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "seat_details")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Seat {
    @Id
    @SequenceGenerator(
            name = "seat_sequence",
            sequenceName = "seat_sequence",
            initialValue = 2000,
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "seat_sequence"
    )
    private Long id;

    @Column
    private String seatNo;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private SeatType seatType;

    @Column(nullable = false)
    private Long seatFare;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private BookingStatus seatStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "flight_id",
            nullable = false,
            referencedColumnName = "id",
            foreignKey = @ForeignKey(
                    name = "Fk_flight_id"
            )
    )
    @JsonIgnore
    private FlightInfo flightInfo;

    @OneToOne(mappedBy = "seatInfo",cascade = CascadeType.ALL)
    private Passenger passenger;

}
