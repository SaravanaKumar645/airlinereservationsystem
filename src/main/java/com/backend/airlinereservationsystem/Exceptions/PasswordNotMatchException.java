package com.backend.airlinereservationsystem.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class PasswordNotMatchException extends RuntimeException{
    public PasswordNotMatchException() {
        super();
    }
    public PasswordNotMatchException(String message) {
        super(message);
    }
    public PasswordNotMatchException(String message, Throwable cause) {
        super(message, cause);
    }
}
