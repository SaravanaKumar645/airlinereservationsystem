package com.backend.airlinereservationsystem.Exceptions;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private final Map<String,Object> apiResponse=new HashMap<>();

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException notValidException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",notValidException
                .getAllErrors()
                .stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
        );
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> authenticationExceptionHandler(AuthenticationException authenticationException){
        apiResponse.put("status",HttpStatus.UNAUTHORIZED.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",authenticationException.getMessage()
        );
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(apiResponse);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<Object> accessDeniedExceptionHandler(AccessDeniedException accessDeniedException){
        apiResponse.put("status",HttpStatus.FORBIDDEN.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",accessDeniedException.getMessage()
        );
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(apiResponse);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Object> userNameNotFoundExceptionHandler(UsernameNotFoundException usernameNotFoundException){
        apiResponse.put("status",HttpStatus.NOT_FOUND.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",usernameNotFoundException.getMessage()
        );
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiResponse);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> resourceNotFoundExceptionHandler(ResourceNotFoundException resourceNotFoundException){
         apiResponse.put("status",HttpStatus.NOT_FOUND.value());
         apiResponse.put("timestamp",new Date());
         apiResponse.put("message",resourceNotFoundException.getMessage());
         apiResponse.put("error",true);
         return ResponseEntity.status(HttpStatus.NOT_FOUND).body(apiResponse);
    }

    @ExceptionHandler(DuplicateEntryException.class)
    public ResponseEntity<Object> duplicateEntryExceptionHandler(DuplicateEntryException duplicateEntryException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",duplicateEntryException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }

    @ExceptionHandler(EmailNotValidException.class)
    public ResponseEntity<Object> emailNotValidExceptionHandler(EmailNotValidException emailNotValidException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",emailNotValidException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }

    @ExceptionHandler(PasswordNotMatchException.class)
    public ResponseEntity<Object> passwordNotMatchExceptionHandler(PasswordNotMatchException passwordNotMatchException){
        apiResponse.put("status",HttpStatus.UNAUTHORIZED.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",passwordNotMatchException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(apiResponse);
    }

    @ExceptionHandler(ResourceNotValidException.class)
    public ResponseEntity<Object> resourceNotValidExceptionHandler(ResourceNotValidException resourceNotValidException){
        apiResponse.put("status",HttpStatus.BAD_REQUEST.value());
        apiResponse.put("timestamp",new Date());
        apiResponse.put("message",resourceNotValidException.getMessage());
        apiResponse.put("error",true);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(apiResponse);
    }




}
