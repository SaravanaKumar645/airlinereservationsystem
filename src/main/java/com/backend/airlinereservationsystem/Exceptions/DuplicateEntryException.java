package com.backend.airlinereservationsystem.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DuplicateEntryException extends RuntimeException{
    public DuplicateEntryException(){
        super();
    }
    public DuplicateEntryException(String message){
        super(message);
    }
    public DuplicateEntryException(String message, Throwable cause) {
        super(message, cause);
    }
}
