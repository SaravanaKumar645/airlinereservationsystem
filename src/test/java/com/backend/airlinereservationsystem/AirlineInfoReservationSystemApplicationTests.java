package com.backend.airlinereservationsystem;

import com.backend.airlinereservationsystem.Entity.FlightInfo;
import com.backend.airlinereservationsystem.Repository.FlightInfoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
class AirlineInfoReservationSystemApplicationTests {
    @Autowired
    private  FlightInfoRepository flightInfoRepository;


    @Test
    void contextLoads() {

    }
    @Test
    public void checker(){
        Optional<FlightInfo> op=flightInfoRepository.findByFlightId(10002L);
        System.out.println(op.get());
    }


}
